(function ($, Drupal, drupalSettings, tarteaucitron) {

  // Geofield Maps Google
  tarteaucitron.services.geofieldmapsgoogle = {
    key: "geofieldmapsgoogle",
    type: "api",
    name: "Geofield Maps Google",
    needConsent: true,
    readmoreLink: "https://tarteaucitron.io/fr/service/googlemaps/",
    uri: "https://www.google.fr/intl/policies/privacy/",
    cookies: [
      "ssm_au_c",
    ],
    js: function ($) {
      "use strict";

      // Convert tacjs altered divs to true geofield-google-map and geofield-map-widget divs
      var divs = document.getElementsByClassName('geofield-google-map-tac');
      for (var div of divs) {
        div.className = div.className.replace('geofield-google-map-tac', 'geofield-google-map');
      }
      divs = document.getElementsByClassName('geofield-map-widget-tac');
      for (div of divs) {
        div.className = div.className.replace('geofield-map-widget-tac', 'geofield-map-widget');
        var mapid = tarteaucitron.getElemAttr(div, 'data-mapid');
        div.setAttribute('id', mapid);
        div.removeAttribute('data-mapid');
      }

      // Re-attach behaviors to load maps
      divs = document.getElementsByClassName('geofield-google-map');
      if (Object.keys(divs).length > 0) {
        Drupal.behaviors.geofieldGoogleMap.attach(document, drupalSettings);
      }
      divs = document.getElementsByClassName('geofield-map-widget');
      if (Object.keys(divs).length > 0) {
        Drupal.behaviors.geofieldMapInit.attach(document, drupalSettings);
      }
    },
    fallback: function () {
      "use strict";
      var id = "geofieldmapsgoogle";
      tarteaucitron.fallback(['geofield-google-map-tac'], tarteaucitron.engage(id));
      tarteaucitron.fallback(['geofield-map-widget-tac'], tarteaucitron.engage(id));
    }
  };

})(jQuery, Drupal, drupalSettings, tarteaucitron);
