/**
 * Google Maps Embed noInner Tarteaucitron service.
 */
(function ($, Drupal, drupalSettings, tarteaucitron) {
  // Google Maps Embed (noInner).
  tarteaucitron.services.googlemapsembednoinner = {
    "key": "googlemapsembednoinner",
    "type": "api",
    "name": "Google Maps (embed noInner)",
    "uri": "https://www.google.fr/intl/policies/privacy/",
    "needConsent": true,
    "cookies": ["DV", "1P_JAR", "CONSENT", "OTZ", "NID", "SEARCH_SAMESITE", "ANID"],
    "js": function () {
      "use strict";
      tarteaucitron.fallback(["googlemapsembednoinner"], function (elem) {

        var iframe = document.createElement("iframe");

        if (elem.id !== "undefined") {
          iframe.id = elem.id;
        }
        iframe.src = elem.dataset.url;

        const width = tarteaucitron.getElemAttr(elem, "width");
        if (width) { iframe.setAttribute("width", width); }
        const height = tarteaucitron.getElemAttr(elem, "height");
        if  (height) { iframe.setAttribute("height", height); }
        const style = tarteaucitron.getElemAttr(elem, "style");
        if (style) { iframe.setAttribute("style", style); }
        const frameborder = tarteaucitron.getElemAttr(elem, "frameborder");
        if (frameborder) { iframe.setAttribute("frameborder", frameborder); }
        const allowfullscreen = tarteaucitron.getElemAttr(elem, "allowfullscreen");
        if (allowfullscreen) { iframe.setAttribute("allowfullscreen", allowfullscreen); }

        elem.parentNode.append(iframe);
        elem.remove();
      }, true);
    },
    "fallback": function () {
      "use strict";

      tarteaucitron.fallback(['googlemapsembednoinner'], tarteaucitron.engage("googlemapsembednoinner"));
    }
  };
})(jQuery, Drupal, drupalSettings, tarteaucitron);
