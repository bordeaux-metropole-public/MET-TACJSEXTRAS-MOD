/**
 * @file
 * Init tacjs_accessibility.
 */

(($, Drupal, drupalSettings, tarteaucitron) => {
  function _replaceElement(elt, newElt) {
    // Create a new element and assign it attributes from the current element
    $.each(elt.attributes, function addAttributes(i, attrib) {
      $(newElt).attr(attrib.name, attrib.value);
    });
    // Replace the current element with the new one and carry over the contents
    $(elt).replaceWith(function addContent() {
      return $(newElt).append($(this).contents());
    });
  }

  /* Il faut attendre que tous les services TarteAuCitron aient bien été initialisés dans la page */
  const loadFunction = tarteaucitron.events.load;
  tarteaucitron.events.load = function accessibilityLoad() {
    if (loadFunction) {
      loadFunction();
    }

    /* Pb avec le titre "Cookies" + texte dans une balise non sémantique (#248 Conservatoire)
     */
    $("<style>")
      .prop("type", "text/css")
      .html("#tarteaucitronDisclaimerAlert::before { content: none; }")
      .appendTo("head");

    /* Problèmes avec les dépliants (#249 Conservatoire)
      Buttons inutiles (#252 Conservatoire) */
    $("#tarteaucitronServices .tarteaucitronDetails").each(
      function forceDisplayBlock() {
        const self = this;
        const targetNode = $(self);
        targetNode.css("max-width", "inherit");
        targetNode.css("position", "inherit");
        targetNode.css("display", "block");
        const observer = new MutationObserver(() => {
          if (self.style.display === "none") {
            targetNode.css("display", "block");
          }
        });
        observer.observe(self, {
          attributes: true,
          attributeFilter: ["style"]
        });
      }
    );
    $(".tarteaucitronPlus").remove();

    /* Boutons “Accepter” / “Refuser” (#250 Conservatoire) */
    $("#tarteaucitronServicesTitle_mandatory .tarteaucitronAllow").attr(
      "aria-pressed",
      true
    );

    /* Structure des contenus (#251 Conservatoire) */
    const serviceContentPath =
      "#tarteaucitronServices .tarteaucitronHidden li.tarteaucitronLine .tarteaucitronName";
    $(serviceContentPath).each(function addP() {
      $(this)
        .children("div")
        .each(function replaceBySpan(e, el) {
          _replaceElement(el, $("<span />"));
        });
      $(this).each(function replaceByP(e, el) {
        _replaceElement(el, $("<p />"));
      });
    });

    /* Buttons inutiles (#252 Conservatoire) */
    $("#tarteaucitronServices .tarteaucitronTitle button")
      .attr("disabled", "")
      .attr("type", "");

    /* aria label sur le bouton de fermeture du panneau de gestion des cookies */
    $("#tarteaucitronBack, #tarteaucitronClosePanel").attr(
      "aria-label",
      "Fermer le panneau de gestion des cookies"
    );

    /* ajout d'un role heading et aria-level au titre cookies dans le panneau cookies */
    $("#tarteaucitronServicesTitle_mandatory div.tarteaucitronTitle")
      .attr("role", "heading")
      .attr("aria-level", "2");

    /* ajout d'un role heading et aria-level au titre cookies dans le bandeau bas cookies */
    $("#tarteaucitronDisclaimerAlert")
      .attr("role", "heading")
      .attr("aria-level", "1");

    // Lot de correctifs pour l'issue suivante :
    // https://git.scnbdx.fr/sid/drupal/modules/com-bootstrap5-theme/-/issues/77
    // ajout d'une balise P autour du texte sous le titre principal de la modale.
    $("#tarteaucitronInfo").wrapInner('<p></p>');
    // ajout d'une balise P autour du texte sous le titre des catégories de service.
    $(".tarteaucitronDetails").wrapInner('<p></p>');
    // suppression role heading sur message cookies obligatoires.
    $("#tarteaucitronServices_mandatory .tarteaucitronH3")
      .removeAttr("role")
      .removeAttr("aria-level");
  };
  // eslint-disable-next-line no-undef
})(jQuery, Drupal, drupalSettings, tarteaucitron);
