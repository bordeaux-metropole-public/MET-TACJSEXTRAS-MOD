<?php

namespace Drupal\tacjs_media_oembed\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media\Plugin\Field\FieldFormatter\OEmbedFormatter;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Plugin implementation of a Tarte Au Citron compatible 'oembed' formatter.
 *
 * @FieldFormatter(
 *   id = "oembed_tac",
 *   label = @Translation("OEmbed content (TAC)"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long",
 *   },
 * )
 */
class OEmbedFormatterTAC extends OEmbedFormatter {

  const TACJS_MEDIA_VIDEO_YOUTUBE_PATTERN =
    '/^https?:\/\/(www\.)?((?!.*list=)youtube\.com\/watch\?.*v=|youtu\.be\/)(?<id>[0-9A-Za-z_-]*)/';
  const TACJS_MEDIA_VIDEO_VIMEO_PATTERN =
    '/^https?:\/\/(www\.)?vimeo\.com\/(channels\/[a-zA-Z0-9]*\/)?(?<id>[0-9]*)(\/[a-zA-Z0-9]+)?(\#t=(\d+)s)?$/';
  const TACJS_MEDIA_VIDEO_DAILYMOTION_PATTERN =
    '/^https?:\/\/(www\.)?dailymotion\.com\/video\/(?<id>[a-z0-9]{6,7})(_([0-9a-zA-Z\-_])*)?$/';
  const TACJS_MEDIA_AUDIO_SOUNDCLOUD_OEMBED_ENDPOINT = 'https://soundcloud.com/oembed?url=';

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = parent::viewElements($items, $langcode);

    if (!\Drupal::service('router.admin_context')->isAdminRoute()
          && \Drupal::routeMatch()->getRouteName() != 'media.filter.preview') {
      $tacjs_settings = \Drupal::config('tacjs.settings');
      foreach ($items as $delta => $item) {
        if ($element[$delta]['#tag'] == 'iframe') {
          $src = $element[$delta]['#attributes']['src'];
          $src_query = parse_url($src, PHP_URL_QUERY);
          parse_str($src_query, $src_params);
          $url = $src_params['url'];
          if (strpos($url, 'youtu') !== FALSE) {
            $tacjs_service = $tacjs_settings->get('services.youtube');
            if (isset($tacjs_service) && !empty($tacjs_service['status'])) {
              $element[$delta]['#tag'] = 'div';
              $element[$delta]['#attributes']['class'][] = 'tac_player';
              $element[$delta]['#attributes']['class'][] = 'youtube_player';
              $element[$delta]['#attributes']['data-videoid'] =
                self::extractVideoID(self::TACJS_MEDIA_VIDEO_YOUTUBE_PATTERN, $url);
              $this->cleanupTacDivAttributes($element[$delta]);
            }
          }
          elseif (strpos($url, 'vimeo') !== FALSE) {
            $tacjs_service = $tacjs_settings->get('services.vimeo');
            if (isset($tacjs_service) && !empty($tacjs_service['status'])) {
              $element[$delta]['#tag'] = 'div';
              $element[$delta]['#attributes']['class'][] = 'tac_player';
              $element[$delta]['#attributes']['class'][] = 'vimeo_player';
              $element[$delta]['#attributes']['data-videoid'] =
                self::extractVideoID(self::TACJS_MEDIA_VIDEO_VIMEO_PATTERN, $url);
              $this->cleanupTacDivAttributes($element[$delta]);
            }
          }
          elseif (strpos($url, 'dailymotion') !== FALSE) {
            $tacjs_service = $tacjs_settings->get('services.dailymotion');
            if (isset($tacjs_service) && !empty($tacjs_service['status'])) {
              $element[$delta]['#tag'] = 'div';
              $element[$delta]['#attributes']['class'][] = 'tac_player';
              $element[$delta]['#attributes']['class'][] = 'dailymotion_player';
              $element[$delta]['#attributes']['data-videoid'] =
                self::extractVideoID(self::TACJS_MEDIA_VIDEO_DAILYMOTION_PATTERN, $url);
              $this->cleanupTacDivAttributes($element[$delta]);
            }
          }
          elseif (strpos($url, 'soundcloud') !== FALSE) {
            $tacjs_service = $tacjs_settings->get('services.soundcloud');
            if (isset($tacjs_service) && !empty($tacjs_service['status'])) {
              $element[$delta]['#tag'] = 'div';
              $element[$delta]['#attributes']['class'][] = 'tac_player';
              $element[$delta]['#attributes']['class'][] = 'soundcloud_player';
              $element[$delta]['#attributes']['data-playable-url'] =
                self::extractSoundcloudPlayableUrl($url);
              $this->cleanupTacDivAttributes($element[$delta]);
            }
          }

        }
      }
    }
    return $element;
  }

  /**
   * @param string $pattern
   * @param string $url
   *
   * @return false|string
   */
  protected static function extractVideoID(string $pattern, string $url) {
    preg_match($pattern, $url, $matches);
    return $matches['id'] ?? FALSE;
  }

  /**
   * Extract a playable URL from result of Soundcloud endpoint request.
   *
   * See also https://developers.soundcloud.com/docs/oembed.
   *
   * @param string $url
   *   URL to get playable URL.
   *
   * @return string
   *   The extracted playable URL.
   */
  protected static function extractSoundcloudPlayableUrl($url) {

    try {
      $oembed_url = self::TACJS_MEDIA_AUDIO_SOUNDCLOUD_OEMBED_ENDPOINT . urlencode($url);
      $response = \Drupal::httpClient()->get($oembed_url, ['headers' => ['Accept' => 'application/json ']]);
      $status_code = $response->getStatusCode();
    }
    catch (GuzzleException $e) {
      \Drupal::logger('tacjs_media_oembed')->warning(
        'Unable to request SoundCloud oEmbed endpoint for url @oembed_url', ['@oembed_url' => $oembed_url]);
      return '';
    }

    if ($status_code != 200) {
      \Drupal::logger('tacjs_media_oembed')->warning(
        'Unable to extract information from Soundcloud oEmbed endpoint for url @url', ['@url' => $url]);
      return '';
    }
    else {
      $body = (string) $response->getBody();
      $json = json_decode($body, TRUE);

      /* Extract src attribute of html value */
      if (!preg_match('/src="([^"]+)"/', $json['html'], $match)) {
        \Drupal::logger('tacjs_media_oembed')->warning(
          'Unable to extract iframe src from Soundcloud oEmbed html data for url @url', ['@url' => $url]);
        return '';
      }
      parse_str(parse_url($match[1])['query'], $output);

      // Return url found in query parameter of extracted src attribute.
      return $output['url'];
    }
  }

  /**
   * Remove or rename attributes from a Tac div element.
   *
   * @param array $element
   *   The iframe element to remove width and height from.
   */
  protected function cleanupTacDivAttributes(array &$element): void {

    unset($element['#attributes']['src']);
    unset($element['#attributes']['frameborder']);
    unset($element['#attributes']['allowtransparency']);
    unset($element['#attributes']['scrolling']);

    if (!empty($element['#attributes']['title'])) {
      $element['#attributes']['data-title'] = $element['#attributes']['title'];
      unset($element['#attributes']['title']);
    }

    if (!empty($element['#attributes']['loading'])) {
      $element['#attributes']['data-loading'] = $element['#attributes']['loading'];
      unset($element['#attributes']['loading']);
    }

    $max_width = $this->getSetting('max_width');
    $max_height = $this->getSetting('max_height');
    // WCAG: keep iframe width/height only if settings are not set to zero.
    if (!empty($max_width) && !empty($element['#attributes']['width'])) {
      $element['#attributes']['data-width'] = $element['#attributes']['width'];
    }
    if (!empty($max_height) && !empty($element['#attributes']['height'])) {
      $element['#attributes']['data-height'] = $element['#attributes']['height'];
    }
    unset($element['#attributes']['width']);
    unset($element['#attributes']['height']);
  }

}
