/**
 * MixCloud Tarteaucitron service
 */
(function ($, Drupal, drupalSettings, tarteaucitron) {
  // MixCloud
  tarteaucitron.services.mixcloud = {
    "key": "mixcloud",
    "type": "video",
    "name": "MixCloud (Podcast)",
    "needConsent": true,
    "cookies": ['__cfduid'],
    "js": function () {
      "use strict";
      tarteaucitron.fallback(['tac_mixcloud'], function (x) {
        var dataSrc = tarteaucitron.getElemAttr(x, 'data-src');
        var dataWidth= tarteaucitron.getElemAttr(x, 'data-width');
        var dataHeight= tarteaucitron.getElemAttr(x, 'data-height');
        var iframeHTML = '<iframe src="' + dataSrc + '" width="' + dataWidth + '" height="' + dataHeight + '" frameBorder="0"';
        if (tarteaucitron.getElemAttr(x, 'data-autoplay') === 'true') {
          iframeHTML += ' allow="autoplay"';
        }
        iframeHTML += '></iframe>';
        return iframeHTML;
      });
    },
    "fallback": function () {
      "use strict";
      var serviceKey = 'mixcloud';
      tarteaucitron.fallback(['tac_mixcloud'], function (elem) {
        elem.style.width = tarteaucitron.getElemAttr(elem, "data-width") + "px";
        elem.style.height = tarteaucitron.getElemAttr(elem, "data-height") + "px";
        return tarteaucitron.engage(serviceKey);
      });
    }
  };
})(jQuery, Drupal, drupalSettings, tarteaucitron);
