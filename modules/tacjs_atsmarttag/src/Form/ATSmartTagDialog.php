<?php

namespace Drupal\tacjs_atsmarttag\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ManageDialog.
 *
 * @package Drupal\tacjs\Form
 */
class ATSmartTagDialog extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tacjs_atsmarttag_dialog';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['atsmarttag.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('atsmarttag.settings');

    $form['collect_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Serveur de collecte'),
      '#description' => $this->t("AT-Internet : Domaine de collecte des données"),
      '#default_value' => $config->get('collect_domain'),
    ];
    $form['collect_domain_ssl'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Serveur de collecte (SSL)'),
      '#description' => $this->t("AT-Internet : Domaine de collecte sécurisé des données"),
      '#default_value' => $config->get('collect_domain_ssl'),
    ];
    $form['site'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Identifiant du site'),
      '#description' => $this->t("AT-Internet : Numéro de site"),
      '#default_value' => $config->get('site'),
    ];
    $form['label_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Libellé de la page'),
      '#description' => $this->t("Ce paramètre détermine comment seront intitulées les pages dans les tableaux de bord d'AT-Internet."),
      '#options' => [
        'page_title' => $this->t('Titre de la page (unicité non garantie)'),
        'path_alias' => $this->t('Chemin de la page (path alias unique)'),
      ],
      '#default_value' => $config->get('label_type') ?? 'path_alias',
    ];
    $form['chapters_from_breadcrumb'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Initialize page chapters from Drupal breadcrumb'),
      '#description' => $this->t("Ce paramètre permet de renseigner les pages de niveau 1, 2 et 3 à partir des éléments du fil d'ariane Drupal (breadcrumb)."),
      '#default_value' => $config->get('chapters_from_breadcrumb') ?? 0,
    ];
    $form['secure'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Secure cookie (https)'),
      '#description' => $this->t("AT-Internet : ce paramètre permet d'envoyer les informations de manière sécurisée ou non. Cocher dans le cas d'un protocole HTTPS et décocher pour un protocole HTTP."),
      '#default_value' => $config->get('secure') ?? 1,
    ];
    $form['disable_cookie'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable tracker cookie'),
      '#description' => $this->t("AT-Internet : ce paramètre, si il est coché, permet d'empêcher l'écriture de cookies du tracker (first et third)."),
      '#default_value' => $config->get('disable_cookie') ?? 1,
    ];
    $form['cookie_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domaine des cookies'),
      '#description' => $this->t("AT-Internet : Domaine pour l'écriture des cookies. Si ce paramètre est vide (cookieDomain: ''), le domaine de la page courante sera utilisé."),
      '#default_value' => $config->get('cookie_domain'),
    ];
    $form['cnil_exempt'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exemption CNIL'),
      '#description' => $this->t("AT-Internet : L'activation du mode hybride exempt de l'autorité CNIL permet une mesure restreinte par le Tracker. Seuls les paramètres jugés « strictement nécessaires » sont collectés."),
      '#default_value' => $config->get('cnil_exempt') ?? 1,
    ];
    $form['need_consent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Consentement requis'),
      '#description' => $this->t("Tarte au citron : pas d'acceptation automatique par défaut du cookie."),
      '#default_value' => $config->get('need_consent') ?? 1,
    ];
    $form['required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Obligatoire'),
      '#description' => $this->t("Ce paramètre permet de forcer l'activation du service en désactivant sa prise en charge par Tarte au citron."),
      '#default_value' => $config->get('required'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('atsmarttag.settings');

    $config
      ->set('collect_domain', $form_state->getValue('collect_domain'))
      ->set('collect_domain_ssl', $form_state->getValue('collect_domain_ssl'))
      ->set('site', $form_state->getValue('site'))
      ->set('label_type', $form_state->getValue('label_type'))
      ->set('chapters_from_breadcrumb', $form_state->getValue('chapters_from_breadcrumb'))
      ->set('secure', $form_state->getValue('secure'))
      ->set('disable_cookie', $form_state->getValue('disable_cookie'))
      ->set('cookie_domain', $form_state->getValue('cookie_domain'))
      ->set('cnil_exempt', $form_state->getValue('cnil_exempt'))
      ->set('need_consent', $form_state->getValue('need_consent'))
      ->set('required', $form_state->getValue('required'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
