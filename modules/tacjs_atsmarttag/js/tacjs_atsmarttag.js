/**
 * AT Internet smarttag service
 */
(function ($, Drupal, drupalSettings, tarteaucitron) {

  if (drupalSettings.atsmarttag !== undefined) {

    var createTagAndDispatch = function () {
      let tag_properties = {
        collectDomain: drupalSettings.atsmarttag.collect_domain,
        collectDomainSSL: drupalSettings.atsmarttag.collect_domain_ssl,
        site: drupalSettings.atsmarttag.site,
      }
      if (drupalSettings.atsmarttag.secure !== 0) {
        tag_properties.secure = true;
      }
      if (drupalSettings.atsmarttag.disable_cookie !== 0) {
        tag_properties.disableCookie = true;
      }
      if (!!drupalSettings.atsmarttag.cookie_domain) {
        tag_properties.cookieDomain = drupalSettings.atsmarttag.cookie_domain;
      }
      window.ATTag = new window.ATInternet.Tracker.Tag(tag_properties);
      if (drupalSettings.atsmarttag.cnil_exempt !== 0) {
        window.ATTag.privacy.setVisitorMode('cnil', 'exempt');
      }
      let page_properties = {};
      if (!!drupalSettings.atsmarttag.page_name) {
        page_properties.name = drupalSettings.atsmarttag.page_name;
      }
      if (!!drupalSettings.atsmarttag.page_chapter1) {
        page_properties.chapter1 = drupalSettings.atsmarttag.page_chapter1;
      }
      if (!!drupalSettings.atsmarttag.page_chapter2) {
        page_properties.chapter2 = drupalSettings.atsmarttag.page_chapter2;
      }
      if (!!drupalSettings.atsmarttag.page_chapter3) {
        page_properties.chapter3 = drupalSettings.atsmarttag.page_chapter3;
      }
      if (Object.getOwnPropertyNames(page_properties).length > 0) {
        window.ATTag.page.set(page_properties);
        window.ATTag.dispatch();
      }
      $(document).ready(function () {
        $(document).trigger('tacjs_atsmarttag:tag_initialized');
      });
    };

    if (drupalSettings.atsmarttag.required) {
      createTagAndDispatch();
    } else {
      // AT Internet SmartTag
      tarteaucitron.services.tacjs_atsmarttag = {
        key: "tacjs_atsmarttag",
        type: "analytic",
        name: "AT Internet",
        uri: "https://helpcentre.atinternet-solutions.com/hc/fr/categories/360002439300-Privacy-Centre",
        needConsent: drupalSettings.atsmarttag.need_consent !== 0,
        cookies: [
          "atidvisitor",
          "atreman",
          "atredir",
          "atsession",
          "atuserid",
          "attvtreman",
          "attvtsession",
        ],
        js: function () {
          "use strict";
          createTagAndDispatch();
        },
      };
    }
  }

})(jQuery, Drupal, drupalSettings, tarteaucitron);
