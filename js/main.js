/**
 * @file
 * Init tacjs_extras.
 */

(function($, Drupal, drupalSettings, tarteaucitron) {

  /**
   * Trigger custom jQuery "loaded" event for custom TacJS services
   * initialization.
   *
   * Can be used in Asset Injector scripts (cf. MEC).
   */
  $(document).trigger('tacjs_extras:tacjs_loaded', [ tarteaucitron ]);

})(jQuery, Drupal, drupalSettings, tarteaucitron);
