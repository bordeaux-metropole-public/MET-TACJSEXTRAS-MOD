/* eslint-disable */
// comarquage, public procedures servicepublic.fr
tarteaucitron.services.comarquage = {
  "key": "comarquage",
  "type": "api",
  "name": "Comarquage (Guide des démarches administratives)",
  "needConsent": true,
  "cookies": [],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(['tac_comarquage'], function (x) {
      var dataId = tarteaucitron.getElemAttr(x, 'data-id');
      var dataPath = tarteaucitron.getElemAttr(x, 'data-path');
      comarquage.container.init({ parentUrl: "/", updateUrl: true });
      comarquage.container.renderCosmetic3Gadget({
        id: dataId, viewParams: { path: dataPath, twoColumns: false, target: "_blank" }, width: "100%"
      });
      return '';
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'comarquage';
    tarteaucitron.fallback(['tac_comarquage'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// dailymotion juke box
tarteaucitron.services.dailymotionjukebox = {
  "key": "dailymotionjukebox",
  "type": "video",
  "name": "Dailymotion juke box (Galerie de vidéos)",
  "needConsent": true,
  "cookies": ['CONSENT', '_ga', '_gid', 'tzo'],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(['tac_dailymotionjukebox'], function (x) {
      var dataId = tarteaucitron.getElemAttr(x, "data-id");
      var dataAllowfullscreen = tarteaucitron.getElemAttr(x, "data-allowfullscreen");
      var dataAllowtransparency = tarteaucitron.getElemAttr(x, "data-allowtransparency");
      var dataStyle = tarteaucitron.getElemAttr(x, "data-style");
      var dataWidth = tarteaucitron.getElemAttr(x, "data-width");
      var dataAlign = tarteaucitron.getElemAttr(x, "data-align");
      var dataFrameborder = tarteaucitron.getElemAttr(x, "data-frameborder");
      var dataMarginwidth = tarteaucitron.getElemAttr(x, "data-marginwidth");
      var dataMarginheight = tarteaucitron.getElemAttr(x, "data-marginheight");
      var dataSrc = tarteaucitron.getElemAttr(x, "data-src");
      return '<iframe id="' + dataId + '" allowfullscreen="' + dataAllowfullscreen + '" allowtransparency="' + dataAllowtransparency + '" style="' + dataStyle + '" width="' + dataWidth + '" align="' + dataAlign + '" frameborder="' + dataFrameborder + '" marginwidth="' + dataMarginwidth + '" marginheight="' + dataMarginheight + '" src="' + dataSrc + '"></iframe>';
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'dailymotionjukebox';
    tarteaucitron.fallback(['tac_dailymotionjukebox'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// dailymotion widget
tarteaucitron.services.dailymotionwidget = {
  "key": "dailymotionwidget",
  "type": "video",
  "name": "Dailymotion widget (Lecteur vidéos)",
  "needConsent": true,
  "cookies": ['e', 'dmxId', 'damd', 'dm-euconsent-v2', 'rtk_session', 'dmvk', 'lang', 'ff', 'UID', '_gid', 'ts', '_ga', 'client_token', 'v1st', 'usprivacy'],
  "js": function () {
    "use strict";
    // when user allows cookies
    (function (w, d, s, u, n, e, c) { w.PXLObject = n; w[n] = w[n] || function () { (w[n].q = w[n].q || []).push(arguments); }; w[n].l = 1 * new Date(); e = d.createElement(s); e.async = 1; e.src = u; c = d.getElementsByTagName(s)[0]; c.parentNode.insertBefore(e, c); });
    tarteaucitron.addScript('//api.dmcdn.net/pxl/client.js', 'pxl');
    tarteaucitron.fallback(['tac_dailymotionwidget'], function (x) {
      var dataPlacement = tarteaucitron.getElemAttr(x, "data-placement");
      return '<div class="dailymotion-widget" data-placement="' + dataPlacement + '"> </div>';
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'dailymotionwidget';
    tarteaucitron.fallback(['tac_dailymotionwidget'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// lightwidget, Instagram images gallery
tarteaucitron.services.lightwidget = {
  "key": "lightwidget",
  "type": "api",
  "name": "Lightwidget (Galerie d'images Instagram)",
  "needConsent": true,
  "cookies": ['__cfduid'],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.addScript('https://cdn.lightwidget.com/widgets/lightwidget.js');
    tarteaucitron.fallback(['tac_lightwidget'], function (x) {
      var dataSrc = tarteaucitron.getElemAttr(x, 'data-src');
      var dataScrolling = tarteaucitron.getElemAttr(x, 'data-scrolling');
      var dataAllowtransparency = tarteaucitron.getElemAttr(x, 'data-allowtransparency');
      var dataClass = tarteaucitron.getElemAttr(x, 'data-class');
      var dataStyle = tarteaucitron.getElemAttr(x, 'data-style');
      return '<iframe src="' + dataSrc + '" scrolling="' + dataScrolling + '" allowtransparency="' + dataAllowtransparency + '" class="' + dataClass + '" style="' + dataStyle + '"></iframe>';
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'lightwidget';
    tarteaucitron.fallback(['tac_lightwidget'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// mailperformance, newletters inscriptions service
tarteaucitron.services.mailperformance = {
  "key": "mailperformance",
  "type": "other",
  "name": "NP6 (Inscription aux infolettres)",
  "needConsent": true,
  "cookies": ['__hssc', '__hssrc', 'hubspotutk', '__hstc', '_fbp', '_gid', '_ga'],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(['tac_mailperformance'], function (x) {
      var dataSrc = tarteaucitron.getElemAttr(x, 'data-src');
      var dataWidth = tarteaucitron.getElemAttr(x, 'data-width');
      var dataHeight = tarteaucitron.getElemAttr(x, 'data-height');
      var dataFrameborder = tarteaucitron.getElemAttr(x, 'data-frameborder');
      var dataScrolling = tarteaucitron.getElemAttr(x, 'data-scrolling');
      return '<iframe src="' + dataSrc + '" width="' + dataWidth + '" height="' + dataHeight + '" frameborder="' + dataFrameborder + '" scrolling="' + dataScrolling + '"></iframe>'
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'mailperformance';
    tarteaucitron.fallback(['tac_mailperformance'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// viewsurf
tarteaucitron.services.viewsurf = {
  "key": "viewsurf",
  "type": "video",
  "name": "Viewsurf (Lecteur vidéos)",
  "needConsent": true,
  "cookies": ['SERVERID43093', 'SRV', 'symfony'],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(['tac_viewsurf'], function (x) {
      var dataSrc = tarteaucitron.getElemAttr(x, 'data-src');
      var dataFrameborder = tarteaucitron.getElemAttr(x, "data-frameborder");
      var dataScrolling = tarteaucitron.getElemAttr(x, 'data-scrolling');
      var dataAllowfullscreen = tarteaucitron.getElemAttr(x, 'data-allowfullscreen');
      var dataStyle = tarteaucitron.getElemAttr(x, 'data-style');
      return '<iframe src="' + dataSrc + '" frameborder="' + dataFrameborder + '" scrolling="' + dataScrolling + '" allowfullscreen="' + dataAllowfullscreen + '" style="' + dataStyle + '"></iframe>';
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'viewsurf';
    tarteaucitron.fallback(['tac_viewsurf'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// Old Xiti
tarteaucitron.services.oldxiti = {
  "key": "oldxiti",
  "type": "analytic",
  "name": "Xiti (AT Internet)",
  "uri": "https://helpcentre.atinternet-solutions.com/hc/fr/categories/360002439300-Privacy-Centre",
  "needConsent": true,
  "cookies": ["xtvrn"],
  "js": function () {
    "use strict";
    if (tarteaucitron.user.oldxitiParams === undefined
      || tarteaucitron.user.oldxitiParams.xtcoreurl === undefined) {
      return;
    }
    window.xtnv = tarteaucitron.user.oldxitiParams.xtnv;
    window.xtsd = tarteaucitron.user.oldxitiParams.xtsd;
    window.xtsite = tarteaucitron.user.oldxitiParams.xtsite;
    window.xtn2 = tarteaucitron.user.oldxitiParams.xtn2;
    window.xtpage = tarteaucitron.user.oldxitiParams.xtpage;
    window.xtdi = tarteaucitron.user.oldxitiParams.xtdi;
    var xt_ac = tarteaucitron.user.oldxitiParams.xt_ac;
    var xt_an = tarteaucitron.user.oldxitiParams.xt_an;
    var xt_multc = tarteaucitron.user.oldxitiParams.xt_multc;
    if (xt_ac !== undefined && xt_an !== undefined && xt_multc !== undefined) {
      if (window.xtparam != null) {
        window.xtparam += "&ac=" + xt_ac + "&an=" + xt_an + xt_multc;
      } else {
        window.xtparam = "&ac=" + xt_ac + "&an=" + xt_an + xt_multc;
      }
    }
    tarteaucitron.addScript(tarteaucitron.user.oldxitiParams.xtcoreurl, '', function () { });
  }
};

// oldaddthis
tarteaucitron.services.oldaddthis = {
  "key": "oldaddthis",
  "type": "social",
  "name": "AddThis",
  "uri": "https://www.addthis.com/privacy/privacy-policy#publisher-visitors",
  "needConsent": true,
  "cookies": ['__atuvc', '__atuvs'],
  "js": function () {
    "use strict";
    if (tarteaucitron.user.oldaddthisPubId === undefined) {
      return;
    }
    tarteaucitron.fallback(['addthis_toolbox'], function (elem) {
      var dataAddThis = tarteaucitron.getElemAttr(elem.parentNode, "data-addthis");
      return dataAddThis == null ? elem.innerHTML : dataAddThis;
    });
    tarteaucitron.addScript('//s7.addthis.com/js/250/addthis_widget.js#pubid=' + tarteaucitron.user.oldaddthisPubId);
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'oldaddthis';
    tarteaucitron.fallback(['addthis_toolbox'], function (elem) {
      elem.parentNode.setAttribute("data-addthis", elem.innerHTML); // backup buttons html in li parent element
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// presdecheznous, la carte collaborative, écologique et solidaire
tarteaucitron.services.presdecheznous = {
  "key": "presdecheznous",
  "type": "api",
  "name": "presdecheznous, la carte collaborative, écologique et solidaire",
  "needConsent": true,
  "cookies": ['address', 'firstVisit', 'viewport'],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(['tac_presdecheznous'], function (x) {
      var url = tarteaucitron.getElemAttr(x, 'data-src'),
        width = tarteaucitron.getElemAttr(x, 'data-width'),
        height = tarteaucitron.getElemAttr(x, 'data-height'),
        marginheight = tarteaucitron.getElemAttr(x, 'data-marginheight'),
        marginwidth = tarteaucitron.getElemAttr(x, 'data-marginwidth'),
        frameborder = tarteaucitron.getElemAttr(x, 'data-frameborder');
      return '<iframe src="' + url + '" width="' + width + '" marginwidth="' + marginwidth + '" marginheight="' + marginheight + '" height="' + height + '" framebroder="' + frameborder + '" scrolling="no"></iframe>';
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'presdecheznous';
    tarteaucitron.fallback(['tac_presdecheznous'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// calameo, livre en pdf
tarteaucitron.services.calameo = {
  "key": "calameo",
  "type": "api",
  "name": "Calameo (Feuilleteur de PDF)",
  "uri": "https://fr.calameo.com/privacy",
  "needConsent": true,
  "cookies": ['_gat_calameo_library', '_gid', 'ckn', '_ga'],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(['tac_calameo'], function (x) {
      var url = tarteaucitron.getElemAttr(x, 'data-src'),
        width = tarteaucitron.getElemAttr(x, 'data-width'),
        height = tarteaucitron.getElemAttr(x, 'data-height'),
        allowfullscreen = tarteaucitron.getElemAttr(x, 'data-allowfullscreen'),
        allowtransparency = tarteaucitron.getElemAttr(x, 'data-allowtransparency'),
        style = tarteaucitron.getElemAttr(x, 'data-style'),
        frameborder = tarteaucitron.getElemAttr(x, 'data-frameborder');
      return '<iframe src="' + url + '" style="' + style + '" width="' + width + '" height="' + height + '" framebroder="' + frameborder + '" scrolling="no"' + 'allowtransparency="' + allowtransparency + '" allowfullscreen="' + allowfullscreen + '"></iframe>';
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'calameo';
    tarteaucitron.fallback(['tac_calameo'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// livresdeproches
tarteaucitron.services.livresdeproches = {
  "key": "livresdeproches",
  "type": "api",
  "name": "livresdeproches, bibliothèque collaborative en ligne",
  "needConsent": true,
  "cookies": [],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(['tac_livresdeproches'], function (x) {
      var dataSrc = tarteaucitron.getElemAttr(x, 'data-src');
      var dataWidth= tarteaucitron.getElemAttr(x, 'data-width');
      var dataHeight= tarteaucitron.getElemAttr(x, 'data-height');
      var dataStyle= tarteaucitron.getElemAttr(x, 'data-style');
      return '<iframe src="' + dataSrc + '" width="' + dataWidth + '" height="' + dataHeight + '" style="' + dataStyle + '"></iframe>'
    });
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'livresdeproches';
    tarteaucitron.fallback(['tac_livresdeproches'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// openagenda
tarteaucitron.services.openagenda = {
  "key": "openagenda",
  "type": "api",
  "name": "openagenda, agenda des activités de la métropole",
  "needConsent": true,
  "cookies": ['oa', 'oa.sig'],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(["tac_openagenda"], function (elem) {
      // Récupération de l'url de l'agenda dans le div TAC
      const oaurl = tarteaucitron.getElemAttr(elem, 'data-oaurl');
      const sitename = tarteaucitron.getElemAttr(elem, 'data-sitename');
      const relatives = tarteaucitron.getElemAttr(elem, 'data-relatives');

      // Conversion en tableau
      const relativeArray = relatives.split(','); // ["upcoming", "current"]
      const queryString = relativeArray.map((value, index) => `relative[${index}]=${encodeURIComponent(value)}`).join('&');

      // Création du blockquote
      const blockquote = document.createElement('blockquote');
      blockquote.className = 'oa-agenda';
      blockquote.setAttribute('align', 'center');
      blockquote.setAttribute('data-base-url', 'oa');
      // Si besoin pourrait utiliser un attribut data-filters qui contiendrait les  valeurs possibles 'search,geo,timings'

      // Création du paragraphe à l'intérieur du blockquote
      const paragraph = document.createElement('p');
      paragraph.setAttribute('lang', 'fr');
      paragraph.innerText = 'Voir les événements de ';

      // Création du lien (a)
      const link = document.createElement('a');
      link.href = oaurl+"?"+queryString;
      link.innerHTML = '<b>'+sitename+'</b>';

      // Ajout du lien au paragraphe
      paragraph.appendChild(link);

      // Ajout du paragraphe au blockquote
      blockquote.appendChild(paragraph);

      // Ajout du blockquote au DOM
      elem.parentNode.append(blockquote);

      // Création et ajout du script de manière asynchrone
      const script = document.createElement('script');
      script.async = true;
      script.src = 'https://cdn.openagenda.com/js/widgets.js';

      // Ajout du script au DOM
      elem.parentNode.append(script);
      elem.remove();
    }, true);
  },
  "fallback": function () {
    "use strict";
    var serviceKey = 'openagenda';
    tarteaucitron.fallback(['tac_openagenda'], function (elem) {
      return tarteaucitron.engage(serviceKey);
    });
  }
};

// facebook
tarteaucitron.services.facebookvideo = {
  "key": "facebookvideo",
  "type": "video",
  "name": "facebook video",
  "needConsent": true,
  "cookies": ['dpr', 'datr'],
  "js": function () {
    "use strict";
    // when user allows cookies
    tarteaucitron.fallback(['tac_facebookvideo'], function (x) {

      var datasrc = tarteaucitron.getElemAttr(x, 'data-src');
      var datastyle = tarteaucitron.getElemAttr(x, 'data-style');
      var dataheight = tarteaucitron.getElemAttr(x, 'data-height');
      var datawidth = tarteaucitron.getElemAttr(x, 'data-width');
      var dataframeborder= tarteaucitron.getElemAttr(x, 'data-frameborder');
      var dataallowfullscreen = tarteaucitron.getElemAttr(x, "data-allowfullscreen");
      var dataAllowtransparency = tarteaucitron.getElemAttr(x, "data-allowtransparency");
      var datascrolling= tarteaucitron.getElemAttr(x, 'data-scrolling');
      var dataallow = tarteaucitron.getElemAttr(x, 'data-allow');

      return '<iframe src="' + datasrc + '" style="' + datastyle + '" height="' + dataheight + '" width="' + datawidth + '"  frameborder="' + dataframeborder + '" allowfullscreen="' + dataallowfullscreen + '" allowtransparency="' + dataAllowtransparency + '" scrolling="' + datascrolling +
        '" allow="' + dataallow + '"></iframe>'
    });
  },
  "fallback": function () {
    "use strict";
    var servicekey = 'facebookvideo';
    tarteaucitron.fallback(['tac_facebookvideo'], function (elem) {
      return tarteaucitron.engage(servicekey);
    });
  }
};

// soundcloud, fonction JS redéfinie pour être accessible, suppression des
// attributs width et height
tarteaucitron.services.soundcloud.js = function () {
  "use strict";
  tarteaucitron.fallback(['soundcloud_player'], function (x) {
    const frame_title = tarteaucitron.getElemAttr(x, 'title') || 'Soundcloud iframe',
      playable_id = tarteaucitron.getElemAttr(x, 'playable-id'),
      playable_type = tarteaucitron.getElemAttr(x, 'playable-type'),
      playable_url = tarteaucitron.getElemAttr(x, 'playable-url'),
      color = tarteaucitron.getElemAttr(x, 'color'),
      autoplay = tarteaucitron.getElemAttr(x, 'auto-play'),
      hideRelated = tarteaucitron.getElemAttr(x, 'hide-related'),
      showComments = tarteaucitron.getElemAttr(x, 'show-comments'),
      showUser = tarteaucitron.getElemAttr(x, 'show-user'),
      showReposts = tarteaucitron.getElemAttr(x, 'show-reposts'),
      showTeaser = tarteaucitron.getElemAttr(x, 'show-teaser'),
      visual = tarteaucitron.getElemAttr(x, 'visual'),
      artwork = tarteaucitron.getElemAttr(x, 'artwork'),
      loading = tarteaucitron.getElemAttr(x, 'loading');

    const allowAutoplay = autoplay === 'true' ? ' allow="autoplay"' : '';

    if (playable_id === undefined && playable_url === undefined) {
      return '';
    }

    // Allow embedding from API results (playable_type + playable_id)
    let qs = '?url=https%3A//api.soundcloud.com/' + playable_type + '/' + playable_id;
    // Or from raw URL from Soundcloud website
    if (playable_url && playable_url.length > 0) qs = '?url=' + escape(playable_url);

    if (hideRelated && hideRelated.length > 0) qs += '&hide_related=' + hideRelated;
    if (color && color.length > 0) qs += '&color=' + color.replace('#', '%23');
    if (autoplay && autoplay.length > 0) qs += '&auto_play=' + autoplay;
    if (showComments && showComments.length > 0) qs += '&show_comments=' + showComments;
    if (hideRelated && hideRelated.length > 0) qs += '&hide_related=' + hideRelated;
    if (showUser && showUser.length > 0) qs += '&show_user=' + showUser;
    if (showReposts && showReposts.length > 0) qs += '&show_reposts=' + showReposts;
    if (showTeaser && showTeaser.length > 0) qs += '&show_teaser=' + showTeaser;
    if (visual && visual.length > 0) qs += '&visual=' + visual;
    if (artwork && artwork.length > 0) qs += '&show_artwork=' + artwork;

    const loading_attr = (loading) ? ' loading="' + loading + '"' : '';

    return '<iframe title="' + Drupal.t('SoundCloud audio: @title', {'@title': frame_title}) + '"' + allowAutoplay + ' src="https://w.soundcloud.com/player/' + qs + '"' + loading_attr + '></iframe>';
  });
};

// youtube, fonction JS redéfinie pour être accessible, suppression des
// attributs width et height.
tarteaucitron.services.youtube.js = function () {
  tarteaucitron.fallback(['youtube_player'], function (x) {
    let frame_title = tarteaucitron.getElemAttr(x, "title") || 'Youtube iframe',
      video_id = tarteaucitron.getElemAttr(x, "videoid"),
      srcdoc = tarteaucitron.getElemAttr(x, "srcdoc"),
      loading = tarteaucitron.getElemAttr(x, "loading"),
      video_width = tarteaucitron.getElemAttr(x, "width"),
      video_height = tarteaucitron.getElemAttr(x, "height"),
      allowfullscreen = tarteaucitron.getElemAttr(x, "allowfullscreen"),
      attrs = ["theme", "rel", "controls", "showinfo", "autoplay", "mute", "start", "end", "loop", "enablejsapi"],
      params = attrs.filter(function (a) {
        return tarteaucitron.getElemAttr(x, a) !== null;
      }).map(function (a) {
        return a + "=" + tarteaucitron.getElemAttr(x, a);
      }).join("&");

    if(tarteaucitron.getElemAttr(x, "loop") === '1') {
      params = params + "&playlist=" + video_id;
    }

    if (video_id === undefined) {
      return "";
    }

    const srcdoc_attr = (srcdoc) ? ' srcdoc="' + srcdoc + '"' : '';
    const loading_attr = (loading) ? ' loading="' + loading + '"' : '';

    if (video_width !== undefined && video_height !== undefined) {
      x.style.width = video_width + 'px';
      x.style.height = video_height + 'px';
    }

    return '<iframe title="' + Drupal.t('YouTube video: @title', {'@title': frame_title}) + '" type="text/html" ' + ' src="//www.youtube-nocookie.com/embed/' + video_id + '?' + params + '"' + (allowfullscreen === '0' ? '' : ' webkitallowfullscreen mozallowfullscreen allowfullscreen') + srcdoc_attr + loading_attr + '></iframe>';
  });
};
tarteaucitron.services.youtube.fallback = function () {
  const id = 'youtube';
  tarteaucitron.fallback(['youtube_player'], function (elem) {
    let video_width = tarteaucitron.getElemAttr(elem, "width"),
      video_height = tarteaucitron.getElemAttr(elem, "height");
    if (video_width !== undefined && video_height !== undefined) {
      elem.style.width = video_width + 'px';
      elem.style.height = video_height + 'px';
    }
    return tarteaucitron.engage(id);
  });
};

// dailymotion, fonction JS redéfinie pour être accessible, suppression des
// attributs width et height.
tarteaucitron.services.dailymotion.js = function () {
  tarteaucitron.fallback(['dailymotion_player'], function (x) {
    const frame_title = tarteaucitron.getElemAttr(x, "title") || 'Dailymotion iframe',
      video_id = tarteaucitron.getElemAttr(x, "videoid"),
      video_width = tarteaucitron.getElemAttr(x, "width"),
      video_height = tarteaucitron.getElemAttr(x, "height"),
      loading = tarteaucitron.getElemAttr(x, "loading"),
      allowfullscreen = tarteaucitron.getElemAttr(x, "allowfullscreen"),
      showinfo = tarteaucitron.getElemAttr(x, "showinfo"),
      autoplay = tarteaucitron.getElemAttr(x, "autoplay"),
      api = tarteaucitron.getElemAttr(x, "api"),
      params = 'info=' + showinfo + '&autoPlay=' + autoplay + '&api=' + api;
    let embed_type = tarteaucitron.getElemAttr(x, "embedType");

    if (video_id === undefined) {
      return "";
    }
    if (embed_type === undefined || !['video', 'playlist'].includes(embed_type)) {
      embed_type = "video";
    }

    const loading_attr = (loading) ? ' loading="' + loading + '"' : '';

    if (video_width !== undefined && video_height !== undefined) {
      x.style.width = video_width + 'px';
      x.style.height = video_height + 'px';
    }

    return '<iframe title="' + Drupal.t('Dailymotion video: @title', {'@title': frame_title}) + '" src="//www.dailymotion.com/embed/' + embed_type + '/' + video_id + '?' + params + '" ' + (allowfullscreen == '0' ? '' : ' webkitallowfullscreen mozallowfullscreen allowfullscreen') + loading_attr + '></iframe>';
  });
};
tarteaucitron.services.dailymotion.fallback = function () {
  const id = 'dailymotion';
  tarteaucitron.fallback(['dailymotion_player'], function (elem) {
    let video_width = tarteaucitron.getElemAttr(elem, "width"),
      video_height = tarteaucitron.getElemAttr(elem, "height");
    if (video_width !== undefined && video_height !== undefined) {
      elem.style.width = video_width + 'px';
      elem.style.height = video_height + 'px';
    }
    return tarteaucitron.engage(id);
  });
};

// vimeo, fonction JS redéfinie pour être accessible, suppression des attributs
// width et height.
tarteaucitron.services.vimeo.js = function () {
    tarteaucitron.fallback(['vimeo_player'], function (x) {
      const frame_title = tarteaucitron.getElemAttr(x, "title") || 'Vimeo iframe',
        video_width = tarteaucitron.getElemAttr(x, "width"),
        video_height = tarteaucitron.getElemAttr(x, "height"),

        video_id = tarteaucitron.getElemAttr(x, "videoid"),
        video_hash = tarteaucitron.getElemAttr(x, "data-hash") || '',
        video_allowfullscreen = tarteaucitron.getElemAttr(x, "data-allowfullscreen"),
        loading = tarteaucitron.getElemAttr(x, "loading"),

        attrs = ["title", "byline", "portrait", "loop", "autoplay", "autopause", "background", "color", "controls", "maxheight", "maxwidth", "muted", "playsinline", "speed", "transparent"];

      let video_qs = "",
        params = attrs.filter(function (a) {
          return tarteaucitron.getElemAttr(x, a) !== null;
        }).map(function (a) {
          return a + "=" + tarteaucitron.getElemAttr(x, a);
        });

      if (video_id === undefined) {
        return "";
      }

      // query params
      if (video_hash.length > 0) {
        params.push("h=" + video_hash);
      }
      if (params.length > 0) {
        video_qs = "?" + params.join("&");
      }

      const loading_attr = (loading) ? ' loading="' + loading + '"' : '';

      if (video_width !== undefined && video_height !== undefined) {
        x.style.width = video_width + 'px';
        x.style.height = video_height + 'px';
      }

      return '<iframe title="' + Drupal.t('Vimeo video: @title', {'@title': frame_title}) + '" src="//player.vimeo.com/video/' + video_id + video_qs + '" ' + (video_allowfullscreen == '0' ? '' : ' webkitallowfullscreen mozallowfullscreen allowfullscreen') + loading_attr + '></iframe>';
    });
  };
  tarteaucitron.services.vimeo.fallback = function () {
    const id = 'vimeo';
    tarteaucitron.fallback(['vimeo_player'], function (elem) {
      let video_width = tarteaucitron.getElemAttr(elem, "width"),
        video_height = tarteaucitron.getElemAttr(elem, "height");
      if (video_width !== undefined && video_height !== undefined) {
        elem.style.width = video_width + 'px';
        elem.style.height = video_height + 'px';
      }
      return tarteaucitron.engage(id);
    });
  };
